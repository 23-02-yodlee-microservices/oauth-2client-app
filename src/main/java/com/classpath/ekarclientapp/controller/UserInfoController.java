package com.classpath.ekarclientapp.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/userinfo")
@RequiredArgsConstructor
@Slf4j
public class UserInfoController {

    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
    @GetMapping
    public Map<String, String> userInfo(OAuth2AuthenticationToken authToken){
        OAuth2User principal = authToken.getPrincipal();
        log.info("Principal :: {}", principal);
        String clientRegistrationId = authToken.getAuthorizedClientRegistrationId();
        log.info("Client registration ID: {}", clientRegistrationId);
        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService.loadAuthorizedClient(clientRegistrationId, principal.getName());
        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
        OAuth2RefreshToken refreshToken = oAuth2AuthorizedClient.getRefreshToken();
        String principalName = oAuth2AuthorizedClient.getPrincipalName();
        Instant issuedAt = accessToken.getIssuedAt();
        Instant expiredAt = accessToken.getExpiresAt();
        String tokenValue = accessToken.getTokenValue();
        Set<String> scopes = accessToken.getScopes();

        Map<String, String> resultMap = new LinkedHashMap<>();
        resultMap.put("Principal ", principalName);
        resultMap.put("Token value ", tokenValue);
        resultMap.put("Issued At ", issuedAt.atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE));
        resultMap.put("Expired At ", expiredAt.atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE));
        resultMap.put("Scopes ", scopes.toString());

        return resultMap;
    }
}
